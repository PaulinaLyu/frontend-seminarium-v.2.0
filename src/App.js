import React from 'react';
import { Route } from 'react-router-dom';
import { MDBContainer, MDBRow, MDBCol } from "mdbreact";
import './App.css';
import CompanyTemplate from './components/Company/CompanyTemplate';
import ProfileTemplate from './components/Profile/ProfileTemplate';
import DashboardTemplate from './components/Dashboard/DashboardTemplate';
import LoginTemplate from './components/Login/LoginTemplate';
import EventTemplate from './components/Event/EventTemplate';

const App = () => {
  	return (
		<>
			<MDBContainer>
  <MDBRow>
					<Route path='/profile/:userId?' 
						render={ () => <ProfileTemplate/> } />

					<Route path='/company/:companyId?' 
						render={ () => <CompanyTemplate /> } />

					<Route path='/dashboard/:userId?' 
						render={ () => <DashboardTemplate /> } />

					<Route path='/event/:eventId?' 
						render={ () => <EventTemplate /> } />
					
					<Route exact path='/' 
						render={ () => <LoginTemplate /> } />
						</MDBRow>
			</ MDBContainer>
  
		</>
  	)
}
export default App;

